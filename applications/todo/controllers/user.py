# -*- coding: utf-8 -*-
# try something like
@auth.requires_login()
def index():
    form = SQLFORM.factory(
    Field('first_name', requires=IS_NOT_EMPTY()),
    Field('last_name', requires=IS_NOT_EMPTY()),
    Field('email', requires=IS_NOT_EMPTY()),
    Field('password', requires=IS_NOT_EMPTY()),
    Field('password_two',
    requires=IS_EQUAL_TO(request.vars.password)))
    if session.history:
        session.history = []
    else:
        session.history = session.history[:20]
    rows = db(db.todo.created_by==auth.user.id).select()
    if form.process().accepted:
        record=db.person(**form.vars)
        if not record:
            db.person.insert(**form.vars)
        rows = db(db.todo.created_by==auth.user.id).select()
        session.history.append(rows)
    return dict(form=form, rows=rows)
